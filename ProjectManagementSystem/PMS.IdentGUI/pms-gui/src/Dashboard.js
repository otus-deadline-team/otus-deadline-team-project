// in src/Dashboard.js
import * as React from "react";
import { Card, CardContent, CardHeader } from '@mui/material';

const Dashboard = () => (
    <Card>
        <CardHeader title="Welcome to the Project Management System" />
        <CardContent>Helo! DeadLine Team  welcome you to use awesome software!)</CardContent>
    </Card>
);

export default Dashboard;