namespace RestClient;

public interface ISmartClient
{
    public void GetAsync();
    public void PostAsync();
    public void PatchAsync();
    public void DeleteAsync();
}