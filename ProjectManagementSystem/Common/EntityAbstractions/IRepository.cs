using EntityAbstractions.Entity;

namespace EntityAbstractions;

public interface IRepository<TEntity> where TEntity: IEntity
{
    TEntity GetItemAsync(Guid id);
    IQueryable<IEntity> GetItemsAsync();
    TEntity CreateAsync();
}