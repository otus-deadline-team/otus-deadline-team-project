﻿using System.ComponentModel;

namespace EntityAbstractions.Entity;

public class Entity: IEntity
{
    public Guid Id { get; set; }
    //Пока нет токенов, данные поля могут быть null
    public Guid? CreatedById { get; set; }
    public string? CreatedBy { get; set; }
    public DateTimeOffset? CreatedAt { get; set; }
    /// <summary>
    /// Запись архивна
    /// </summary>
    [DefaultValue(false)]
    public bool IsArchive { get; set; }
}