﻿namespace EntityAbstractions.Entity;

public interface IEntity
{
    public Guid Id { get; set; }
}