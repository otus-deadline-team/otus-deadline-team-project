﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace PMS.Identity.App.Infrastucture
{
    public interface IAuthOptions
    {
        string Issuer { get; set; } 
        string Audience { get; set; }   
        string Key { get; set; }
        int LifeTime { get; set; }  
        SymmetricSecurityKey GetSymmetricSecurityKey();
        
    }
}
