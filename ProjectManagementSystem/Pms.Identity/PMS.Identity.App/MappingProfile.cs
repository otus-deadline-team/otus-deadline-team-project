﻿using AutoMapper;
using PMS.Identity.Client.ApiModels.User;

namespace PMS.Identity.App
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {        
            CreateMap<Models.Entities.User, UserApiModel>();
            CreateMap<UserApiModel, Models.Entities.User>();
            CreateMap<Models.Entities.User, UserCreateApiModel>();
            CreateMap<UserCreateApiModel, Models.Entities.User>();
            CreateMap<Models.Entities.User, UserPatchApiModel>();
            CreateMap<UserPatchApiModel, Models.Entities.User>();
            CreateMap<Models.Entities.User, UserAuthorizeApiModel>();
            CreateMap<UserAuthorizeApiModel, Models.Entities.User>();
            CreateMap<Models.Entities.User, UserJwtTokenModel>();
            CreateMap<UserJwtTokenModel, Models.Entities.User>();
        }
    }
}
