﻿using Microsoft.EntityFrameworkCore;
using PMS.Identity.App.Models.EntityConfigurations;

namespace PMS.Identity.App.Context
{
    public class IdentityContext : DbContext 
    {
        public IdentityContext(DbContextOptions<IdentityContext> options)
            : base(options)
        {     
        }

        public DbSet<Models.Entities.User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {           
            modelBuilder.HasDefaultSchema("identity");
            modelBuilder.UseSerialColumns();
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

    }



}
