﻿using PMS.Identity.Client.ApiModels.User;


namespace PMS.Identity.App.User.BL.Abstractions
{
    public interface IUserRepository
    {
        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>UserApiModel</returns>
        public Task<UserApiModel?> GetUser(Guid id);

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns>UserApiModel</returns>
        public Task<List<UserApiModel>> GetUsers();

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="createApiModel">new user</param>
        /// <returns>UserApiModel</returns>
        public Task<UserApiModel> CreateAsync(UserCreateApiModel createApiModel);


        /// <summary>
        /// Update user by id
        /// </summary>
        /// <param name="user">user</param>
        /// <returns></returns>
        public Task<bool> PatchAsync(Guid id, UserPatchApiModel user);
    
        /// <summary>
        /// Delete user by id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public Task<bool> DeleteAsync(Guid id);

        /// <summary>
        /// Authorize user by name and pass
        /// </summary>
        /// <param name="user">user</param>
        /// <returns></returns>
        public Task<UserJwtTokenModel> AuthorizeUser(UserAuthorizeApiModel user);


    }
}
