﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PMS.Identity.App.Infrastucture;
using PMS.Identity.App.User.BL.Abstractions;
using PMS.Identity.Client;
using PMS.Identity.Client.ApiModels.User;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.OAuth;
using PMS.Identity.App.Infrastructure;
using Microsoft.AspNetCore.Cors;
using System.Collections.Generic;

namespace PMS.Identity.App.User.Controllers;

[EnableCors("_myAllowSpecificOrigins")]
[ApiController]
public class UserController : ControllerBase
{

    private readonly IUserRepository _userRepository;
    private readonly IAuthOptions _authOptions;

    public UserController(IUserRepository userRepository, IAuthOptions authOptions)
    {
        _userRepository = userRepository;
        _authOptions = authOptions; 
    }

    [HttpGet(IdentityRouting.GetUser)]
    public async Task<UserApiModel?> GetUser([FromRoute] Guid id)
    {
        return await _userRepository.GetUser(id);
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpGet(IdentityRouting.GetAllUsers)]
    public async Task<List<UserApiModel>> GetAllUsers()
    {
        var res= await _userRepository.GetUsers();
        Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
        Response.Headers.Add("X-Total-Count", res.Count.ToString());
        return res;
    }

    [HttpPost(IdentityRouting.CreateUser)]
    public async Task<ActionResult<UserApiModel>>CreateUser([FromBody] UserCreateApiModel user)
    {
        try
        {
            var res = await _userRepository.CreateAsync(user);
            return Ok(res);
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message); 
        }
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpPut(IdentityRouting.PatchUser)]
    public async Task<ActionResult> PatchUser([FromRoute] Guid id, [FromBody] UserPatchApiModel user)
    {
        
        try
        {
            var task = await _userRepository.PatchAsync(id, user);
            return Ok(task);
        }
        catch (ArgumentException exx)
        {
            return NotFound(exx.Message);
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpDelete(IdentityRouting.DeleteUser)]
    public async Task<ActionResult> DeleteUser([FromRoute] Guid id)
    {
        try
        {
            var res = await _userRepository.DeleteAsync(id);
            return Ok(true);
        }
        catch (ArgumentException exx)
        {
            return NotFound(exx.Message);
        }
        catch (Exception ex)
        {
            return StatusCode(500, ex.Message);
        }
    }


    [HttpPost(IdentityRouting.GetToken)]
    public async Task<IActionResult> Token([FromBody] UserAuthorizeApiModel user)
    {
        var identity =await GetIdentity(user);
        if (identity == null)
        {
            return BadRequest(new { errorText = "Invalid username or password." });
        }

        var encodedJwt = TokenProducer.GetJWTToken(identity.Claims, _authOptions);

        var response = new
        {
            token = encodedJwt,
            username = identity.Name
        };

        return Ok(response);
    }

    private async Task<ClaimsIdentity> GetIdentity(UserAuthorizeApiModel user)
    {
        var person = await _userRepository.AuthorizeUser(user);
        if (person != null)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, person.UserName),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, person.Role),
                    new Claim("ID", person.Id.ToString()),
                    new Claim("NameTelegram", person.TelegramUserName.ToString()),                  
                    new Claim("Role", person.Role)
                };
            ClaimsIdentity claimsIdentity =
            new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
            return claimsIdentity;
        }

        // если пользователя не найдено
        return null;
    }

}
