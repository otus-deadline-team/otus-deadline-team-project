﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PMS.Identity.App.Context;
using PMS.Identity.App.Infrastucture;
using PMS.Identity.App.User.BL.Abstractions;
using PMS.Identity.App.User.BL.Implementation;
using Swashbuckle.AspNetCore.SwaggerGen;
using JwtBearerOptions = PMS.Identity.App.Infrastucture.JwtBearerOptions;

namespace PMS.Identity.App;

public static class Registration
{
    public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();

        services.AddSwaggerGen(SwaggerOptions.SwaggerGenOptions(new SwaggerGenOptions(), configuration));

        services.AddDbContext<IdentityContext>(opt => opt.UseNpgsql(configuration.GetConnectionString("ModuleDatabase"),
            x => x.MigrationsHistoryTable("__MigrationHistory", "identity")));
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MappingProfile());
        });
        IMapper mapper = mapperConfig.CreateMapper();
        services.AddSingleton(mapper);

        services.AddTransient<IUserRepository, UserRepository>();

        //Добавляем авторизацию
        IAuthOptions authOptions=new AuthOptions(configuration);
        services.AddSingleton(authOptions);
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(JwtBearerOptions.GetJwtBearerOptions(new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerOptions(),configuration,authOptions));


        //При старте приложения запускаем миграции
        using var serviceProvider = services.BuildServiceProvider();
        using var context = serviceProvider.GetRequiredService<IdentityContext>();
        context.Database.Migrate();
    }
}

