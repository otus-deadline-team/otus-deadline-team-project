﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PMS.Identity.App.Migrations
{
    public partial class AddMailIsActiveToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("0a5a2905-b5e4-44be-9e2d-316030832d9e"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("24f09285-3a25-4064-b174-7b7fd8a799d6"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("2b810825-ffb5-4066-a069-88be689eff8f"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("bc3dbe25-4ced-4d23-9f69-a44d28774aa8"));

            migrationBuilder.AddColumn<string>(
                name: "Email",
                schema: "identity",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "identity",
                table: "Users",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                schema: "identity",
                table: "Users",
                columns: new[] { "Id", "Email", "IsActive", "PasswordHash", "Role", "Salt", "TelegramUserName", "UserName" },
                values: new object[,]
                {
                    { new Guid("502c91c4-1f77-417b-8f3a-7abf08861931"), "user@mail.ru", true, "VL9rUzqtok8CRdXTffr0IGvT1OtRbyqpieJzwNblet0=", "User", "YlToLuOUr54qDm57PO+j1w==", "@user", "User" },
                    { new Guid("80df4e69-92be-4cfe-a1c4-05d6a89d6c08"), "sys@mail.ru", true, "VL9rUzqtok8CRdXTffr0IGvT1OtRbyqpieJzwNblet0=", "System", "YlToLuOUr54qDm57PO+j1w==", "@system", "System" },
                    { new Guid("df4a2623-2f92-4c6f-899b-6a56071523c8"), "sash@mail.ru", true, "gIH/y5g0+vu5bYgjNj1sx0DlerCwA4Mhv/3Kpb+5ZcA=", "Administrator", "bTPSr6wCe4j0ELvN0cEhvQ==", "@sash", "Sash" },
                    { new Guid("ea6f193c-402b-4e56-b89d-6a0a7eed33ea"), "dan@mail.ru", true, "VL9rUzqtok8CRdXTffr0IGvT1OtRbyqpieJzwNblet0=", "Administrator", "YlToLuOUr54qDm57PO+j1w==", "@dan", "Dan" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("502c91c4-1f77-417b-8f3a-7abf08861931"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("80df4e69-92be-4cfe-a1c4-05d6a89d6c08"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("df4a2623-2f92-4c6f-899b-6a56071523c8"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("ea6f193c-402b-4e56-b89d-6a0a7eed33ea"));

            migrationBuilder.DropColumn(
                name: "Email",
                schema: "identity",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "identity",
                table: "Users");

            migrationBuilder.InsertData(
                schema: "identity",
                table: "Users",
                columns: new[] { "Id", "PasswordHash", "Role", "Salt", "TelegramUserName", "UserName" },
                values: new object[,]
                {
                    { new Guid("0a5a2905-b5e4-44be-9e2d-316030832d9e"), "THizjInZipJT/7EvZ1QPbQ9yPpS/p6onY1T56YMdbXU=", "Administrator", "B8FObD1hIvICEJlBPQZb9A==", "@dan", "Dan" },
                    { new Guid("24f09285-3a25-4064-b174-7b7fd8a799d6"), "THizjInZipJT/7EvZ1QPbQ9yPpS/p6onY1T56YMdbXU=", "System", "B8FObD1hIvICEJlBPQZb9A==", "@system", "System" },
                    { new Guid("2b810825-ffb5-4066-a069-88be689eff8f"), "o9jIzJfdcxN580Y96T+fZMpY76xpfouQQLPvCivHi/s=", "Administrator", "J41y9xajjSbr+063vhPE5w==", "@sash", "Sash" },
                    { new Guid("bc3dbe25-4ced-4d23-9f69-a44d28774aa8"), "THizjInZipJT/7EvZ1QPbQ9yPpS/p6onY1T56YMdbXU=", "User", "B8FObD1hIvICEJlBPQZb9A==", "@user", "User" }
                });
        }
    }
}
