﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PMS.Identity.App.Migrations
{
    public partial class UserModelAddRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("243e091b-ca0a-4388-bc1e-aa91440d0450"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("f860d073-0bf9-4fd3-b76f-8a808e8b4acf"));

            migrationBuilder.AddColumn<string>(
                name: "Role",
                schema: "identity",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                schema: "identity",
                table: "Users",
                columns: new[] { "Id", "PasswordHash", "Role", "Salt", "TelegramUserName", "UserName" },
                values: new object[,]
                {
                    { new Guid("0a5a2905-b5e4-44be-9e2d-316030832d9e"), "THizjInZipJT/7EvZ1QPbQ9yPpS/p6onY1T56YMdbXU=", "Administrator", "B8FObD1hIvICEJlBPQZb9A==", "@dan", "Dan" },
                    { new Guid("24f09285-3a25-4064-b174-7b7fd8a799d6"), "THizjInZipJT/7EvZ1QPbQ9yPpS/p6onY1T56YMdbXU=", "System", "B8FObD1hIvICEJlBPQZb9A==", "@system", "System" },
                    { new Guid("2b810825-ffb5-4066-a069-88be689eff8f"), "o9jIzJfdcxN580Y96T+fZMpY76xpfouQQLPvCivHi/s=", "Administrator", "J41y9xajjSbr+063vhPE5w==", "@sash", "Sash" },
                    { new Guid("bc3dbe25-4ced-4d23-9f69-a44d28774aa8"), "THizjInZipJT/7EvZ1QPbQ9yPpS/p6onY1T56YMdbXU=", "User", "B8FObD1hIvICEJlBPQZb9A==", "@user", "User" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("0a5a2905-b5e4-44be-9e2d-316030832d9e"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("24f09285-3a25-4064-b174-7b7fd8a799d6"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("2b810825-ffb5-4066-a069-88be689eff8f"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("bc3dbe25-4ced-4d23-9f69-a44d28774aa8"));

            migrationBuilder.DropColumn(
                name: "Role",
                schema: "identity",
                table: "Users");

            migrationBuilder.InsertData(
                schema: "identity",
                table: "Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "TelegramUserName", "UserName" },
                values: new object[,]
                {
                    { new Guid("243e091b-ca0a-4388-bc1e-aa91440d0450"), "XZt3NeZhAPi7Rdlna2nyHr4n+hetC6XjYBgrY0hCoj4=", "1/ghklHic6meveEpM3GzJA==", "@sash", "Саша" },
                    { new Guid("f860d073-0bf9-4fd3-b76f-8a808e8b4acf"), "mfq/JpfmhWzkWdSAw7qHhvL+TU0UnW5IB0aaBvuUbE0=", "yXDlztvY7H/4fkS1oBR56g==", "@dan", "Даня" }
                });
        }
    }
}
