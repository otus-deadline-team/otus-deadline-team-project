﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PMS.Identity.App.Migrations
{
    public partial class UserModelUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("441b2a72-1652-4387-a87a-65c121d7e8ae"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("98ec33f9-efdf-472f-a560-b89e9ea4e345"));

            migrationBuilder.AddColumn<string>(
                name: "PasswordHash",
                schema: "identity",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Salt",
                schema: "identity",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "TelegramUserName",
                schema: "identity",
                table: "Users",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                schema: "identity",
                table: "Users",
                columns: new[] { "Id", "PasswordHash", "Salt", "TelegramUserName", "UserName" },
                values: new object[,]
                {
                    { new Guid("243e091b-ca0a-4388-bc1e-aa91440d0450"), "XZt3NeZhAPi7Rdlna2nyHr4n+hetC6XjYBgrY0hCoj4=", "1/ghklHic6meveEpM3GzJA==", "@sash", "Саша" },
                    { new Guid("f860d073-0bf9-4fd3-b76f-8a808e8b4acf"), "mfq/JpfmhWzkWdSAw7qHhvL+TU0UnW5IB0aaBvuUbE0=", "yXDlztvY7H/4fkS1oBR56g==", "@dan", "Даня" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("243e091b-ca0a-4388-bc1e-aa91440d0450"));

            migrationBuilder.DeleteData(
                schema: "identity",
                table: "Users",
                keyColumn: "Id",
                keyValue: new Guid("f860d073-0bf9-4fd3-b76f-8a808e8b4acf"));

            migrationBuilder.DropColumn(
                name: "PasswordHash",
                schema: "identity",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Salt",
                schema: "identity",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "TelegramUserName",
                schema: "identity",
                table: "Users");

            migrationBuilder.InsertData(
                schema: "identity",
                table: "Users",
                columns: new[] { "Id", "UserName" },
                values: new object[,]
                {
                    { new Guid("441b2a72-1652-4387-a87a-65c121d7e8ae"), "Даня" },
                    { new Guid("98ec33f9-efdf-472f-a560-b89e9ea4e345"), "Саша" }
                });
        }
    }
}
