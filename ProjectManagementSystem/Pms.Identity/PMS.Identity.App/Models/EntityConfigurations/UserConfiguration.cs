﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PMS.Identity.App.User.BL.Utils;

namespace PMS.Identity.App.Models.EntityConfigurations
{   

    public class UserConfiguration: IEntityTypeConfiguration<Entities.User>
    {
       public void Configure(EntityTypeBuilder<Entities.User> builder)
       {
        builder.HasKey(p => p.Id);
        builder.Property(p => p.UserName).IsRequired().HasMaxLength(200);

        var testPas1=userHelper.GetSaltAndPassword("123456");
        var testPas2=userHelper.GetSaltAndPassword("123456");

        // Для примера, позже удалить
        builder.HasData(
            new Entities.User() {Id = Guid.NewGuid(), UserName = "Sash", TelegramUserName="@sash", Salt=testPas1.salt, PasswordHash=testPas1.pass ,Role="Administrator", Email="sash@mail.ru" ,IsActive=true},
            new Entities.User() {Id = Guid.NewGuid(), UserName = "Dan", TelegramUserName="@dan", Salt=testPas2.salt, PasswordHash=testPas2.pass, Role="Administrator", Email = "dan@mail.ru", IsActive = true },
            new Entities.User() {Id = Guid.NewGuid(), UserName = "System", TelegramUserName = "@system", Salt = testPas2.salt, PasswordHash = testPas2.pass, Role = "System", Email = "sys@mail.ru", IsActive = true },
            new Entities.User() { Id = Guid.NewGuid(), UserName = "User", TelegramUserName = "@user", Salt = testPas2.salt, PasswordHash = testPas2.pass, Role = "User", Email = "user@mail.ru", IsActive = true }
        );
       }
    }

 
}
