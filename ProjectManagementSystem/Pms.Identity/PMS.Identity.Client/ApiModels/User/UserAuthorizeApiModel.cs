﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Identity.Client.ApiModels.User
{
    public class UserAuthorizeApiModel
    {
        public string UserName { get; set; }       

        public string Password { get; set; }
    }
}
