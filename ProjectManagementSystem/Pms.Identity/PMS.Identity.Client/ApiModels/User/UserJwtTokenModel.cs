﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMS.Identity.Client.ApiModels.User;

public class UserJwtTokenModel
{
    public Guid Id { get; set; }

    public string UserName { get; set; }

    public string Role { get; set; } = "User";

    public string TelegramUserName { get; set; }
}