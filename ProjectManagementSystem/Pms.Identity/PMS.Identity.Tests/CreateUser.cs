using Newtonsoft.Json;
using Npgsql;
using PMS.Identity.Client.ApiModels.User;
using System.Collections;
using System.Diagnostics;
using System.Net;
using System.Net.Http;

namespace PMS.Identity.Tests;

public class CreateUser
{
    [Fact]
    public void StressTest()
    {
        /// ������� threadCount �������, � ������ �������� usersCountCreateInThread ��� ��� �������� �������������. 
        /// ���� ��� ��� ������� ��� 200 ���� �������.

        int threadCount = 4; // ���-�� �������
        int usersCountCreateInThread = 10; // ���-�� ������, ����������� � ����� ������
        string testUserName = "testUser";
        UserCreateApiModel testUser = CreateTestUserApiModel(testUserName);
        
        List<UsersPostWork> UsersCreateWorker = new List< UsersPostWork >();
        for (int i = 0; i < threadCount; i++)
        {
            UsersPostWork uw = new UsersPostWork(usersCountCreateInThread);
            UsersCreateWorker.Add(uw);
            uw.newThread.Start(testUser);
        }

        foreach (var work in UsersCreateWorker)
            work.newThread.Join();

        bool assertRule = true;
        foreach (var work in UsersCreateWorker)
            assertRule = assertRule && work.AllStatusIsOk;

        int rowsDel = DeleteTestUsers(testUserName);

        Assert.True(assertRule && (rowsDel == threadCount* usersCountCreateInThread));

    }

    /// <summary>
    /// ������� ������������ ��� �����
    /// </summary>
    /// <param name="name">���</param>
    /// <returns></returns>
    UserCreateApiModel CreateTestUserApiModel(string name)
    {
        UserCreateApiModel user = new UserCreateApiModel();
        user.UserName = name;
        user.TelegramUserName = string.Format("{0}Telegram", name); 
        user.Email = string.Format("{0}@{0}", name); 
        user.Password = name;
        return user;
    }

    /// <summary>
    /// ����� ������� ����� � � ��� ��������� �������� �������������
    /// </summary>
    public class UsersPostWork
    {
        public bool AllStatusIsOk = true;
        public int CountUserToAdd = 10;// ���-�� ������, ����������� � ������
        public Thread newThread;

        public UsersPostWork()
        {
        }
        public UsersPostWork(int countUserToAdd)
        {
            CountUserToAdd = countUserToAdd;
            newThread = new Thread(DoWork);
        }

        public void DoWork(object data)
        {
            for (int i = 0; i < CountUserToAdd; i++)
            {
                HttpStatusCode code = PostUser(data).Result.StatusCode;
                if (AllStatusIsOk)
                    AllStatusIsOk = (code == HttpStatusCode.OK);
            }
        }

        public async Task<HttpResponseMessage> PostUser(object user)
        {
            string jsonString = JsonConvert.SerializeObject(user);
            StringContent httpContent = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");

            using HttpClient client = new HttpClient();

            var response = await client.PostAsync("https://localhost:7250/user", httpContent);
            return response;
        }
    }

    /// <summary>
    /// ������� �������� ������������� �� ����
    /// </summary>
    /// <param name="userName"></param>
    /// <returns></returns>
    int DeleteTestUsers(string userName)
    {
        const string connectString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=postgres";
        int rows = -1;
        using (Npgsql.NpgsqlConnection connectionInProj = new Npgsql.NpgsqlConnection(connectString))
        {
            connectionInProj.Open();
            string sqlDeleteTestUsers = String.Format("DELETE FROM identity.\"Users\" WHERE \"UserName\" = \'{0}\'", userName);
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: sqlDeleteTestUsers, connection: connectionInProj))
            {
                rows = command.ExecuteNonQuery();
            }
        }
        return rows;
    }
}