﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using PMS.Notification.App.Context;

#nullable disable

namespace PMS.Notification.App.Migrations
{
    [DbContext(typeof(NotificationContext))]
    [Migration("20220814221437_AddBlogCreatedTimestamp")]
    partial class AddBlogCreatedTimestamp
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasDefaultSchema("notification")
                .HasAnnotation("ProductVersion", "6.0.6")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseSerialColumns(modelBuilder);

            modelBuilder.Entity("PMS.Notification.App.Models.Entities.History", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("Message")
                        .IsRequired()
                        .HasMaxLength(300)
                        .HasColumnType("character varying(300)");

                    b.Property<string>("ReceiverUserName")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("character varying(50)");

                    b.HasKey("Id");

                    b.ToTable("Histories", "notification");

                    b.HasData(
                        new
                        {
                            Id = new Guid("489b3279-21a0-448c-a2a1-da31adcb8d63"),
                            Message = "Вам назначена задача",
                            ReceiverUserName = "Gobzzzila"
                        },
                        new
                        {
                            Id = new Guid("8b03135a-226d-4b34-b164-f92b94620704"),
                            Message = "Вас добавили в проект OTUS TEAM",
                            ReceiverUserName = "Gobzzzila"
                        });
                });

            modelBuilder.Entity("PMS.Notification.App.Models.Entities.TelegramUserData", b =>
                {
                    b.Property<long>("ChatId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseSerialColumn(b.Property<long>("ChatId"));

                    b.Property<string>("Username")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("ChatId");

                    b.ToTable("TelegramUsersData", "notification");

                    b.HasData(
                        new
                        {
                            ChatId = 123123123L,
                            Username = "Example"
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
