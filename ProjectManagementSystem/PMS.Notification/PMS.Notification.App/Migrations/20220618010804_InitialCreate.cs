﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PMS.Notification.App.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "notification");

            migrationBuilder.CreateTable(
                name: "Histories",
                schema: "notification",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Message = table.Column<string>(type: "character varying(300)", maxLength: 300, nullable: false),
                    ReceiverUserName = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Histories", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "notification",
                table: "Histories",
                columns: new[] { "Id", "Message", "ReceiverUserName" },
                values: new object[,]
                {
                    { new Guid("489b3279-21a0-448c-a2a1-da31adcb8d63"), "Вам назначена задача", "Gobzzzila" },
                    { new Guid("8b03135a-226d-4b34-b164-f92b94620704"), "Вас добавили в проект OTUS TEAM", "Gobzzzila" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Histories",
                schema: "notification");
        }
    }
}
