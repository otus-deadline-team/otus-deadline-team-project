﻿using AutoMapper;
using PMS.Notification.Client.ApiModels.Histories;

namespace PMS.Notification.App;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Models.Entities.History, HistoryApiModel>();
    }
}