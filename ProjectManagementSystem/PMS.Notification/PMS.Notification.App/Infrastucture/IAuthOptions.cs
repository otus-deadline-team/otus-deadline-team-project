﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace PMS.Notification.App.Infrastructure
{
    public interface IAuthOptions
    {
        string Issuer { get; set; } 
        string Audience { get; set; }   
        string Key { get; set; }
        int LifeTime { get; set; }  
        SymmetricSecurityKey GetSymmetricSecurityKey();
        
    }
}
