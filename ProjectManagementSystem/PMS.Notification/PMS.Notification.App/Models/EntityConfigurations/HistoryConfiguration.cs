﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace PMS.Notification.App.Models.EntityConfigurations;

public class HistoryConfiguration : IEntityTypeConfiguration<Entities.History>
{
    public void Configure(EntityTypeBuilder<Entities.History> builder)
    {
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Message).IsRequired().HasMaxLength(300);
        builder.Property(p => p.ReceiverUserName).IsRequired().HasMaxLength(50);

        // Для примера, позже удалить
        builder.HasData(
            new Entities.History() { Id = new Guid("489B3279-21A0-448C-A2A1-DA31ADCB8D63"), Message = "Вам назначена задача", ReceiverUserName = "Gobzzzila"},
            new Entities.History() { Id = new Guid("8B03135A-226D-4B34-B164-F92B94620704"), Message = "Вас добавили в проект OTUS TEAM", ReceiverUserName = "Gobzzzila" }
        );
    }
}