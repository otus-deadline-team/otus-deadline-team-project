﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PMS.Notification.App.Models.Entities;

namespace PMS.Notification.App.Models.EntityConfigurations;

public class TelegramUserDataConfiguration : IEntityTypeConfiguration<TelegramUserData>
{
    public void Configure(EntityTypeBuilder<TelegramUserData> builder)
    {
        builder.HasKey(p => p.ChatId);
        builder.Property(p => p.Username).IsRequired();

        builder.HasData(
            new TelegramUserData() { ChatId = 123123123, Username ="example"}
        );
    }
}