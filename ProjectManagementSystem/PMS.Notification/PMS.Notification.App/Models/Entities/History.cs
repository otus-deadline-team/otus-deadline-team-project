﻿using EntityAbstractions.Entity;

namespace PMS.Notification.App.Models.Entities;

public class History : IEntity
{
    public Guid Id { get; set; }

    public string Message { get; set; }
 
    public string ReceiverUserName { get; set; }
}