﻿namespace PMS.Notification.App.Models.Entities;

public class TelegramUserData
{
    public long ChatId { get; set; }
    public string Username { get; set; }
}