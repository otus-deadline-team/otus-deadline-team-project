﻿using PMS.Notification.App.History.BL.Impl;
using PMS.Notification.Client.ApiModels.Histories;

namespace PMS.Notification.App.History.BL.Abstractions;

public interface IHistoryRepository
{
    Task<HistoryApiModel?> GetSpecificHistoryRecord(Guid id);
    Task<List<HistoryApiModel>> GetFullHistory();
    Task<HistoryApiModel> CreateAsync(HistoryCreateApiModel createApiModel);

    //Not Implemented 
    // public Task PatchAsync();
    // public Task DeleteAsync();
}