﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMS.Notification.App.Context;
using PMS.Notification.App.History.BL.Abstractions;
using PMS.Notification.Client.ApiModels.Histories;

namespace PMS.Notification.App.History.BL.Impl;

public class HistoryRepository : IHistoryRepository
{
    private readonly NotificationContext _context;
    private readonly IMapper _mapper;
    public HistoryRepository(NotificationContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    public async Task<HistoryApiModel?> GetSpecificHistoryRecord(Guid id)
    {
        var entity = await _context.Histories.FirstOrDefaultAsync(x => x.Id == id);
        if (entity != null)
        {
            return _mapper.Map<HistoryApiModel>(entity);
        }
        return null;
    }

    public async Task<List<HistoryApiModel>> GetFullHistory()
    {
        var entities = await _context.Histories.ToListAsync();
        return _mapper.Map<List<HistoryApiModel>>(entities);
    }

    public async Task<HistoryApiModel> CreateAsync(HistoryCreateApiModel createApiModel)
    {
        try
        {
            var item = new Models.Entities.History
            {
                Id = Guid.NewGuid(),
                Message = createApiModel.Message,
                ReceiverUserName = createApiModel.ReceiverUserName
            };
            await _context.Histories.AddAsync(item);
            await _context.SaveChangesAsync();

            return _mapper.Map<HistoryApiModel>(item);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public Task PatchAsync()
    {
        throw new NotImplementedException();
    }

    public Task DeleteAsync()
    {
        throw new NotImplementedException();
    }
}