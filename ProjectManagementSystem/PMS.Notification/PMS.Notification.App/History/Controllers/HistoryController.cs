﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PMS.Notification.App.Infrastructure;
using PMS.Notification.Client;
using PMS.Notification.Client.ApiModels.Histories;
using System.Data;

namespace PMS.Notification.App.History.Controllers;

[ApiController]
public class HistoryController : ControllerBase
{
    private readonly BL.Abstractions.IHistoryRepository _historyRepository;

    public HistoryController(BL.Abstractions.IHistoryRepository historyRepository)
    {
        _historyRepository = historyRepository;
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpGet(HistoryRouting.GetSpecificHistoryRecord)]
    public async Task<HistoryApiModel?> GetSpecificHistoryRecord([FromRoute] Guid id)
    {
        return await _historyRepository.GetSpecificHistoryRecord(id);
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpGet(HistoryRouting.GetFullHistory)]
    public async Task<List<HistoryApiModel>> GetFullHistory()
    {
        return await _historyRepository.GetFullHistory();
    }
}