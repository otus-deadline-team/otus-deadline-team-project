using PMS.Notification.App;

var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddJsonFile("identitysettings.json");
builder.Configuration.AddJsonFile("swaggersettings.json");

Registration.ConfigureServices(builder.Services, builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();   // добавление middleware аутентификации 

app.UseAuthorization();   // добавление middleware авторизации 

app.MapControllers();

app.Run();