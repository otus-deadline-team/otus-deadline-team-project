﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using PMS.Notification.App.Context;
using PMS.Notification.App.History.BL.Abstractions;
using PMS.Notification.App.History.BL.Impl;
using PMS.Notification.App.Infrastructure;
using PMS.Notification.App.Notification.BL.Abstractions;
using PMS.Notification.App.Notification.BL.Impl;
using Swashbuckle.AspNetCore.SwaggerGen;
using JwtBearerOptions = PMS.Notification.App.Infrastructure.JwtBearerOptions;

namespace PMS.Notification.App;

public static class Registration
{
    public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(SwaggerOptions.SwaggerGenOptions(new SwaggerGenOptions(), configuration));
        services.AddDbContext<NotificationContext>(opt => opt.UseNpgsql(configuration.GetConnectionString("ModuleDatabase"),
            x => x.MigrationsHistoryTable("__MigrationHistory", "notification")));
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MappingProfile());
        });
        IMapper mapper = mapperConfig.CreateMapper();
        services.AddSingleton(mapper);

        var telegramBot = new TelegramBot(services);
        services.AddSingleton<ITelegramBot>(telegramBot);

        services.AddTransient<IHistoryRepository, HistoryRepository>();
        services.AddTransient<ITelegramUserDataRepository, TelegramUserDataRepository>();

        services.AddHostedService<RabbitMqListener>();

        //Добавляем авторизацию
        IAuthOptions authOptions = new AuthOptions(configuration);
        services.AddSingleton(authOptions);
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(JwtBearerOptions.GetJwtBearerOptions(new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerOptions(), configuration, authOptions));

        //При старте приложения запускаем миграции
        using var serviceProvider = services.BuildServiceProvider();
        using var context = serviceProvider.GetRequiredService<NotificationContext>();
        context.Database.Migrate();
    }
}