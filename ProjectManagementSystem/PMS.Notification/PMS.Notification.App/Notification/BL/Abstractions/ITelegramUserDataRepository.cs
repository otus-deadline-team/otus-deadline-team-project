﻿using PMS.Notification.App.Models.Entities;

namespace PMS.Notification.App.Notification.BL.Abstractions;

public interface ITelegramUserDataRepository
{
    Task CreateAsync(long userId, string username);

    Task<string> CheckUsernameAsync(string username);
}