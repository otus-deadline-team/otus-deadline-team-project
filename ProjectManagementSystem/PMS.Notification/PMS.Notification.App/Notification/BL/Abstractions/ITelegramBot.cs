﻿namespace PMS.Notification.App.Notification.BL.Abstractions;

public interface ITelegramBot 
{
    Task SendCustomMessage(string message, string receiver);
}