﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PMS.Notification.App.Context;
using PMS.Notification.App.Models.Entities;
using PMS.Notification.App.Notification.BL.Abstractions;

namespace PMS.Notification.App.Notification.BL.Impl;

public class TelegramUserDataRepository : ITelegramUserDataRepository
{
    private readonly NotificationContext _context;
    private readonly IMapper _mapper;

    public TelegramUserDataRepository(NotificationContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task CreateAsync(long chatId, string username)
    {
        var entity = await _context.TelegramUsersData.FindAsync(chatId);
        if (entity != null)
            return;

        await _context.TelegramUsersData.AddAsync(new TelegramUserData
        {
            ChatId = chatId,
            Username = username.ToLower()
        });

        await _context.SaveChangesAsync();
    }

    public async Task<string> CheckUsernameAsync(string username)
    {
        var chatId = await _context.TelegramUsersData.FirstOrDefaultAsync(x => x.Username == username.ToLower());
        return chatId == null ? username : chatId.ChatId.ToString();
    }
}