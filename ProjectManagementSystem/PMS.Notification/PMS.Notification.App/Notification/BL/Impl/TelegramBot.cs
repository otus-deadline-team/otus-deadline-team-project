﻿using PMS.Notification.App.History.BL.Abstractions;
using PMS.Notification.App.Notification.BL.Abstractions;
using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;

namespace PMS.Notification.App.Notification.BL.Impl;

public class TelegramBot : ITelegramBot
{
    private readonly IServiceCollection _services;
    private ITelegramBotClient bot = new TelegramBotClient("5481035575:AAFKCY5MEB3xhwuTTqFT7PwbPuGlk-PDxZg");

    public TelegramBot(IServiceCollection services)
    {
        _services = services;

        var cts = new CancellationTokenSource();
        var cancellationToken = cts.Token;
        var receiverOptions = new ReceiverOptions
        {
            AllowedUpdates = { }
        };
        bot.StartReceiving(HandleUpdateAsync, HandleErrorAsync, receiverOptions, cancellationToken);
    }

    private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
    {
        if (update.Type == Telegram.Bot.Types.Enums.UpdateType.Message)
        {
            var message = update.Message;
            
            if (message.Text.ToLower() == "/start")
            {
                var userDataRepository = TelegramUserDataRepositoryResolver();
                await userDataRepository.CreateAsync(update.Message.Chat.Id, update.Message.Chat.Username);
                await botClient.SendTextMessageAsync(message.Chat, "Добро пожаловать!");
            }
        }
    }

    private async Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
    {
        
    }

    public async Task SendCustomMessage(string message, string receiver)
    {
        var userDataRepository = TelegramUserDataRepositoryResolver();
        var checkedChatId = await userDataRepository.CheckUsernameAsync(receiver);
        await bot.SendTextMessageAsync(checkedChatId, message);
    }

    private ITelegramUserDataRepository TelegramUserDataRepositoryResolver()
    {
        var serviceProvider = _services.BuildServiceProvider();
        return serviceProvider.GetRequiredService<ITelegramUserDataRepository>();
    }
}