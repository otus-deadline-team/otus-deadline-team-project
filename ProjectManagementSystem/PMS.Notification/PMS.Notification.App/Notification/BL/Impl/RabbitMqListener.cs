﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using Newtonsoft.Json;
using PMS.Notification.App.History.BL.Abstractions;
using PMS.Notification.App.Notification.BL.Abstractions;
using PMS.Notification.Client.ApiModels.Histories;
using PMS.Notification.Client.ApiModels.Notification;

namespace PMS.Notification.App.Notification.BL.Impl;

public class RabbitMqListener : BackgroundService
{
    private readonly ITelegramBot _telegramBot;

    //private readonly IHistoryRepository _historyRepository;
    private IConnection _connection;
    private IModel _channel;

    public RabbitMqListener(ITelegramBot telegramBot/*, IHistoryRepository historyRepository*/)
    {
        _telegramBot = telegramBot;
        //_historyRepository = historyRepository;

        var factory = new ConnectionFactory
        {
            UserName = "prvtowue",
            Password = "RgRUq9NtTqVF3YhVsWjfpGryZFj3FMQs",
            VirtualHost = "prvtowue",
            HostName = "hawk-01.rmq.cloudamqp.com"
        };
        _connection = factory.CreateConnection();
        _channel = _connection.CreateModel();
        //_channel.QueueDeclare(queue: "TelegramNotificationsQueue");
    }

    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        stoppingToken.ThrowIfCancellationRequested();

        var consumer = new EventingBasicConsumer(_channel);
        consumer.Received += OnConsumerOnReceived!;

        _channel.BasicConsume("TelegramNotificationsQueue", false, consumer);

        return Task.CompletedTask;
    }

    private void OnConsumerOnReceived(object ch, BasicDeliverEventArgs ea)
    {
        var content = JsonConvert.DeserializeObject<TgNotifRabbitMessage>(Encoding.UTF8.GetString(ea.Body.ToArray()));

        if (content != null)
        {
            _telegramBot.SendCustomMessage(content.Message, content.Receiver);
            //_historyRepositoryFactory.CreateHistoryRepository().CreateAsync(new HistoryCreateApiModel
            //{
            //    Message = content.Message,
            //    ReceiverUserName = content.Receiver
            //});
        }

        _channel.BasicAck(ea.DeliveryTag, false);
    }

    public override void Dispose()
    {
        _channel.Close();
        _connection.Close();
        base.Dispose();
    }
}