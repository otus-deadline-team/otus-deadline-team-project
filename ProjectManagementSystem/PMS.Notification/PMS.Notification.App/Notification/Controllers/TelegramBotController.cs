﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PMS.Notification.App.Infrastructure;
using PMS.Notification.App.Notification.BL.Abstractions;
using PMS.Notification.Client;

namespace PMS.Notification.App.Notification.Controllers;

[ApiController]
public class TelegramBotController : ControllerBase
{
    private readonly ITelegramBot _telegramBot;

    public TelegramBotController(ITelegramBot telegramBot)
    {
        _telegramBot = telegramBot;
    }

    [RequirePrivelege(Priviliges.System,Priviliges.Administrator)]
    [HttpPost(TelegramBotRouting.GetFullHistory)]
    public async Task SendCustomNotification([FromQuery] string message, [FromQuery] string receiver)
    {
        await _telegramBot.SendCustomMessage(message, receiver);
    }
    //-1001571270623
}