﻿using Microsoft.EntityFrameworkCore;
using PMS.Notification.App.Models.Entities;
using PMS.Notification.App.Models.EntityConfigurations;

namespace PMS.Notification.App.Context;

public class NotificationContext : DbContext
{
    public NotificationContext(DbContextOptions<NotificationContext> options) : base(options)
    {
    }

    public DbSet<Models.Entities.History> Histories { get; set; }
    public DbSet<TelegramUserData> TelegramUsersData { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("notification");
        modelBuilder.UseSerialColumns();
        modelBuilder.ApplyConfiguration(new HistoryConfiguration());
        modelBuilder.ApplyConfiguration(new TelegramUserDataConfiguration());
    }
}