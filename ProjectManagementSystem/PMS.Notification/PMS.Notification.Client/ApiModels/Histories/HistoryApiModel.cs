﻿namespace PMS.Notification.Client.ApiModels.Histories;

public class HistoryApiModel
{
    public Guid Id { get; set; }

    public string Message { get; set; }

    public string ReceiverUserName { get; set; }
}