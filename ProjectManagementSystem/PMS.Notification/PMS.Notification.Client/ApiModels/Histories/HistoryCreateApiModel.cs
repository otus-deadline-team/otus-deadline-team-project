﻿namespace PMS.Notification.Client.ApiModels.Histories;

public class HistoryCreateApiModel
{
    public string Message { get; set; }

    public string ReceiverUserName { get; set; }
}