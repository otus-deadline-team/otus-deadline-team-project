﻿namespace PMS.Notification.Client.ApiModels.Notification;

public class TgNotifRabbitMessage
{
    public string Receiver { get; set; }
    public string Message { get; set; }
}