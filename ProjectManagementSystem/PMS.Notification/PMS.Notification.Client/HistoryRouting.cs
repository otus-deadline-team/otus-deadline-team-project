﻿namespace PMS.Notification.Client;

public class HistoryRouting
{
    public const string GetFullHistory = "histories";
    public const string GetSpecificHistoryRecord = "histories/{id}";
    public const string CreateProject = "histories";
    public const string PatchProject = "histories/{id}";
    public const string DeleteProject = "histories/{id}";
}