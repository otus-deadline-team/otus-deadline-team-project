using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Pms.Core.App;
using Pms.Core.Client.ApiModels.Project;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      builder =>
                      {
                          builder.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin();   //SetIsOriginAllowed(x=>true).AllowCredentials()
                      });
});


builder.Configuration.AddJsonFile("identitysettings.json");
builder.Configuration.AddJsonFile("swaggersettings.json");
builder.Configuration.AddJsonFile("rabbitoptions.json");

Registration.ConfigureServices(builder.Services, builder.Configuration);

var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors(MyAllowSpecificOrigins);

app.UseAuthentication();   // добавление middleware аутентификации 

app.UseAuthorization();   // добавление middleware авторизации 

app.MapControllers();


//app.UseMiddleware<CustomMiddleware>();
app.UseMiddleware<MiddlewareCore>();


app.Run();

