﻿using Newtonsoft.Json;
using Pms.Core.App.RabbitMQ;
using Pms.Core.App.Telegram.BL.Abstractions;

namespace Pms.Core.App.Telegram.BL.Impl
{
    public class TelegramService : ITelegramService
    {
        private readonly IRabbitMqService _mqService;
        public TelegramService(IRabbitMqService mqService)
        {
            _mqService = mqService;
        }
        public void SendToTelegram(object obj, string nameTg)
        {
            TelegramSender ts = new TelegramSender(_mqService);
            string messageToSend = JsonConvert.SerializeObject(obj);
            ts.SendMessageToRabbit(messageToSend, nameTg);
        }
    }
}
