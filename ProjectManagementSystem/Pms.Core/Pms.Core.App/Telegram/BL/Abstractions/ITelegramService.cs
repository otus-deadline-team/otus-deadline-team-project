﻿namespace Pms.Core.App.Telegram.BL.Abstractions
{
    public interface ITelegramService
    {
        /// <summary>
        /// Отправка сообщений в телеграм
        /// </summary>
        /// <param name="obj">объект передаваемый в тело сообщения</param>
        /// <param name="nameTg">ник в телеграм кому отправляяем</param>
        public void SendToTelegram(object obj, string nameTg);
    }
}
