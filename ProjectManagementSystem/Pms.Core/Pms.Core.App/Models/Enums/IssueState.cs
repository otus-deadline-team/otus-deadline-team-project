namespace Pms.Core.App.Models.Enums;

public enum IssueState
{
    BACKLOG,
    IN_PROGRESS,
    COMPLETE
}