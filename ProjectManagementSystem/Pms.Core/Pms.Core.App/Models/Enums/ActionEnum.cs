﻿namespace Pms.Core.App.Models.Enums
{
    //
    public enum ActionEnum
    {
        NotFound = 0, // объект не найден
        WasDone = 1,  // функция отработала корректно (delete, patch)
    }
}
