using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pms.Core.App.Models.EntityConfigurations;

public class IssueConfigurations : IEntityTypeConfiguration<Entities.Issue>
{
    public void Configure(EntityTypeBuilder<Entities.Issue> builder)
    {
        builder.HasKey(i => i.Id);
        builder.Property(i => i.Title).IsRequired();

        builder.Property(i => i.State)
            .HasConversion<string>();
        
        builder.HasOne(i => i.ParentIssue)
            .WithMany(i => i.SubIssues)
            .HasForeignKey(i => i.ParentIssueId)
            .OnDelete(DeleteBehavior.SetNull);

        builder.HasOne(p => p.Project)
            .WithMany(p => p.Issues)
            .HasForeignKey(i => i.ProjectId)
            .OnDelete(DeleteBehavior.SetNull);

        // ��� �������, ����� �������
        builder.HasData(
            new Entities.Issue() { Id = Guid.NewGuid(), Title = "�������� ������ 1",Description="����������� ������", State =Enums.IssueState.BACKLOG, ProjectId= new Guid("0f8fad5b-d9cb-469f-a165-70867728950e") },
            new Entities.Issue() { Id = Guid.NewGuid(), Title = "�������� ������ 2", Description = "����������� ������", State = Enums.IssueState.BACKLOG, ProjectId = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e") }
        );
    }
}