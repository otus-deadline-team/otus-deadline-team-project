using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Pms.Core.App.Models.EntityConfigurations;

public class ProjectConfiguration : IEntityTypeConfiguration<Entities.Project>
{
    public void Configure(EntityTypeBuilder<Entities.Project> builder)
    {
        builder.HasKey(p => p.Id);
        builder.Property(p => p.Name).IsRequired().HasMaxLength(200);
        // Для примера, позже удалить
        builder.HasData(
            new Entities.Project() {Id = new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"), Name = "C# Professional"},
            new Entities.Project() {Id = new Guid("7c9e6679-7425-40de-944b-e07fc1f90ae7"), Name = "C# Basic"}
        );
    }
}