using EntityAbstractions.Entity;
using Pms.Core.App.Models.Enums;

namespace Pms.Core.App.Models.Entities;

public class Issue : Entity
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public Guid? ResponsibleId { get; set; }
    public IssueState State { get; set; }
    
    public Guid? ParentIssueId { get; set; }
    
    public Issue? ParentIssue { get; set; }
    
    public Guid? ProjectId { get; set; }
    public Project? Project { get; set; }
    
    public List<Issue> SubIssues { get; set; }
}