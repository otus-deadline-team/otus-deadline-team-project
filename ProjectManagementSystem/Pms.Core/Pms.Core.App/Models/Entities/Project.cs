using EntityAbstractions.Entity;

namespace Pms.Core.App.Models.Entities;

public class Project : Entity
{
    public string Name { get; set; }

    public List<Issue> Issues { get; set; }
}