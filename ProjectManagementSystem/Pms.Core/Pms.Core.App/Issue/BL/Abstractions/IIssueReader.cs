using Pms.Core.Client.ApiModels.Issue;

namespace Pms.Core.App.Issue.BL.Abstractions;

public interface IIssueReader
{
    Task<IssueApiModel> GetIssueByIdAsync(Guid issueId);
    Task<List<IssueApiModel>> GetIssuesByProjectIdAsync(Guid projectId);
};