using Pms.Core.Client.ApiModels.Issue;

namespace Pms.Core.App.Issue.BL.Abstractions;

public interface IIssuePatcher
{
    Task<IssueApiModel> PatchAsync(Guid issueId, IssuePatchApiModel patchApiModel);
    Task<IssueApiModel> ChangeStateAsync(Guid issueId, IssueStateApiEnum issueStateApiEnum);
}