namespace Pms.Core.App.Issue.BL.Abstractions;

public interface IIssueRemover
{
    Task RemoveAsync(Guid issueId);
}