using Pms.Core.Client.ApiModels.Issue;
using System.Security.Claims;

namespace Pms.Core.App.Issue.BL.Abstractions;

public interface IIssueCreator
{
    //TODO После реализации Сохранение проектов поле сделать обязательным
    Task<IssueApiModel> CreateAsync( IssueCreateApiModel issueCreateApiModel, ClaimsIdentity userInfo);
}