using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Pms.Core.App.Common;
using Pms.Core.App.Context;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.App.Models.Enums;
using Pms.Core.App.SystemWorker.BL.Abstraction;
using Pms.Core.App.Telegram.BL.Abstractions;
using Pms.Core.Client.ApiModels.Issue;
using PMS.Identity.Client.ApiModels.User;

namespace Pms.Core.App.Issue.BL.Impl;

public class IssueCreator : IIssueCreator
{
    private readonly CoreContext _context;
    private readonly IMapper _mapper;
    private readonly ICommonService _identityCommonService;
    private readonly ISystemWorker _systemWorker;

    public IssueCreator(CoreContext context, IMapper mapper, ICommonService identityCommonService, ISystemWorker systemWorker)
    {
        _context = context;
        _mapper = mapper;
        _identityCommonService = identityCommonService;
        _systemWorker = systemWorker;
    }
    
    public async Task<IssueApiModel> CreateAsync(IssueCreateApiModel issueCreateApiModel, ClaimsIdentity userInfo)
    {
        var newIssue = _mapper.Map<Models.Entities.Issue>(issueCreateApiModel);
        var checkProject = await _context.Projects.FirstOrDefaultAsync(p => p.Id == issueCreateApiModel.ProjectId);
        if (checkProject == null)
            throw new ValidationException("ProjectId is Empty");
        newIssue.Project = checkProject;
        if (issueCreateApiModel.ParentIssueId.HasValue)
        {
            var parentIssue =await _context.Issues.FirstOrDefaultAsync(x => x.Id == issueCreateApiModel.ParentIssueId);
            if (parentIssue != null)
            {
                if (parentIssue.ProjectId != issueCreateApiModel.ProjectId)
                    throw new ValidationException("Different projects");
                newIssue.ParentIssue = parentIssue;
            }
        }
        newIssue.State = IssueState.BACKLOG;
        newIssue.CreatedById = new Guid(userInfo.Claims.FirstOrDefault(a => a.Type == "ID").Value);
        newIssue.CreatedBy = userInfo.Name;
        newIssue.CreatedAt = DateTimeOffset.UtcNow;
        var result = await _context.Issues.AddAsync(newIssue);
        await _context.SaveChangesAsync();
        if (issueCreateApiModel.ResponsibleId != null)
        {
            UserApiModel responsibleUser = _identityCommonService.Get<UserApiModel>($"user/{issueCreateApiModel.ResponsibleId}?id={issueCreateApiModel.ResponsibleId}");
            if (responsibleUser != null && !string.IsNullOrEmpty(responsibleUser.TelegramUserName))
            {
                string textToResponsible = $"��� ��������� ������ {issueCreateApiModel.Title}";
                _systemWorker.SendMessage(textToResponsible, responsibleUser.TelegramUserName);
            }
        }
        return _mapper.Map<IssueApiModel>(result.Entity);
    }
}