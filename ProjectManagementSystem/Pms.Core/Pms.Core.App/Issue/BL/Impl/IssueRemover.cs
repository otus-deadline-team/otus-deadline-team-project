using Microsoft.EntityFrameworkCore;
using Pms.Core.App.Context;
using Pms.Core.App.Issue.BL.Abstractions;

namespace Pms.Core.App.Issue.BL.Impl;

public class IssueRemover : IIssueRemover
{
    private readonly CoreContext _context;
    public IssueRemover(CoreContext context) => _context = context;
    public async Task RemoveAsync(Guid issueId)
    {
        var changingIssue = await _context.Issues.FirstOrDefaultAsync(i => i.Id == issueId);
        if (changingIssue == null)
            throw new ArgumentException("Issue not found");
        _context.Issues.Remove(changingIssue);
        await _context.SaveChangesAsync();
    }
}