using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Pms.Core.App.Context;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.App.Models.Enums;
using Pms.Core.Client.ApiModels.Issue;

namespace Pms.Core.App.Issue.BL.Impl;

public class IssuePatcher : IIssuePatcher
{
    private readonly CoreContext _context;
    private readonly IMapper _mapper;

    public IssuePatcher(CoreContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    public async Task<IssueApiModel> PatchAsync(Guid issueId, IssuePatchApiModel patchApiModel)
    {
        var changingIssue = await _context.Issues.FirstOrDefaultAsync(i => i.Id == issueId);
        if (changingIssue == null)
            throw new ArgumentException("Issue not found");
        changingIssue.ResponsibleId = patchApiModel.ResponsibleId;
        changingIssue.Description = patchApiModel.Description;
        changingIssue.Title = patchApiModel.Title;
        changingIssue.State = _mapper.Map<IssueState>(patchApiModel.IssueState);
        if (patchApiModel.ProjectId != changingIssue.ProjectId)
        {
            if (await _context.Projects.AnyAsync(p => p.Id == patchApiModel.ProjectId))
            {
                changingIssue.ProjectId = patchApiModel.ProjectId;
                foreach (var subIssue in changingIssue.SubIssues)
                    subIssue.ProjectId = patchApiModel.ProjectId;
            }
            throw new ArgumentException("Project not found");
        }
        
        //if (patchApiModel.ParentIssueId != changingIssue.ParentIssueId
        //    && await _context.Issues.AnyAsync(i=>i.Id == patchApiModel.ParentIssueId))
        //{
            changingIssue.ParentIssueId = patchApiModel.ParentIssueId;
        //}
        //else
        //{
        //    throw new ArgumentException("New Parent Issue doesn't exist");
        //}

        var res = _context.Issues.Update(changingIssue);
        await _context.SaveChangesAsync();

        return _mapper.Map<IssueApiModel>(res.Entity);
    }

    public async Task<IssueApiModel> ChangeStateAsync(Guid issueId, IssueStateApiEnum issueStateApiEnum)
    {
        var changingIssue = await _context.Issues.FirstOrDefaultAsync(i => i.Id == issueId);
        if (changingIssue == null)
            throw new ArgumentException("Issue not found");
        changingIssue.State = _mapper.Map<IssueState>(issueStateApiEnum);
        var res = _context.Update(changingIssue);
        await _context.SaveChangesAsync();
        return _mapper.Map<IssueApiModel>(res.Entity);
    }
}