using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Pms.Core.App.Context;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.Client.ApiModels.Issue;

namespace Pms.Core.App.Issue.BL.Impl;

public class IssueReader : IIssueReader
{
    private readonly CoreContext _context;
    private readonly IMapper _mapper;
    public IssueReader(CoreContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    public async  Task<IssueApiModel> GetIssueByIdAsync(Guid issueId)
    {
        var result = await _context.Issues.FirstOrDefaultAsync(i => i.Id == issueId);
        if (result == null)
            throw new ArgumentException("Issue not found");
        return _mapper.Map<IssueApiModel>(result);
    }

    public async Task<List<IssueApiModel>> GetIssuesByProjectIdAsync(Guid projectId)
    {
        var projectExist = await _context.Projects.AnyAsync(p => p.Id == projectId);
        if (!projectExist)
            throw new ArgumentException("Project not Found");
        var issues = await _context.Issues.Where(i => i.ProjectId == projectId).ToListAsync();
        return _mapper.Map<List<IssueApiModel>>(issues);
    }
}