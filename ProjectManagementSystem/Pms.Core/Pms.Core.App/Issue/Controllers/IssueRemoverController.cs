using Microsoft.AspNetCore.Mvc;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.Client;
using PMS.Core.App.Infrastucture;

namespace Pms.Core.App.Issue.Controllers;

[ApiController]
public class IssueRemoverController : ControllerBase
{
    private readonly IIssueRemover _issueRemover;

    public IssueRemoverController(IIssueRemover issueRemover)
    {
        _issueRemover = issueRemover;
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpDelete(CoreRouting.Issues.Remove)]
    public async Task<ActionResult> RemoveIssue([FromQuery] Guid issueId)
    {
        try
        {
            await _issueRemover.RemoveAsync(issueId);
            return Ok();
        }
        catch (Exception e)
        {
            return Problem(statusCode: 500, detail: e.Message);
        }
    }
}