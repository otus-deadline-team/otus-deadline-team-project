using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.Client;
using Pms.Core.Client.ApiModels.Issue;
using PMS.Core.App.Infrastucture;

namespace Pms.Core.App.Issue.Controllers;

[ApiController]
public class IssueCreatorController : ControllerBase
{
    private readonly IIssueCreator _issueCreator;

    public IssueCreatorController(IIssueCreator issueCreator)
    {
        _issueCreator = issueCreator;
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpPost(CoreRouting.Issues.Create)]
    public async Task<ActionResult<IssueApiModel>> CreateAsync([FromBody] IssueCreateApiModel createApiModel)
    {
        try
        {
            ClaimsIdentity userInfo = User.Identities.FirstOrDefault();
            if (userInfo != null)
            {
                var result = await _issueCreator.CreateAsync(createApiModel, userInfo);
                return Ok(result);
            }
            else
            {
                throw new Exception("������������ �� ���������.");
            }
        }
        catch (ValidationException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(statusCode: 500, detail: ex.Message);
        }
    }
}