using Microsoft.AspNetCore.Mvc;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.Client;
using Pms.Core.Client.ApiModels.Issue;
using PMS.Core.App.Infrastucture;

namespace Pms.Core.App.Issue.Controllers;

[ApiController]
public class IssueReaderController : ControllerBase
{
    private readonly IIssueReader _issueReader;
    public IssueReaderController(IIssueReader issueReader)
    {
        _issueReader = issueReader;
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System, Priviliges.User, Priviliges.Spectrator)]
    [HttpGet(CoreRouting.Issues.Get)]
    public async Task<ActionResult<IssueApiModel>> GetIssue([FromQuery] Guid issueId)
    {
        try
        {
            var result = await _issueReader.GetIssueByIdAsync(issueId);
            return Ok(result);
        }
        catch (Exception e)
        {
            return Problem(statusCode: 500, detail: e.Message);
        }
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System,Priviliges.User, Priviliges.Spectrator)]
    [HttpGet(CoreRouting.Issues.GetAllByProjectId)]
    public async Task<ActionResult<IssueApiModel>> GetProjectIssues([FromQuery] Guid projectId)
    {
        try
        {
            var result = await _issueReader.GetIssuesByProjectIdAsync(projectId);
            return Ok(result);
        }
        catch (Exception e)
        {
            return Problem(statusCode: 500, detail: e.Message);
        }
    }
}