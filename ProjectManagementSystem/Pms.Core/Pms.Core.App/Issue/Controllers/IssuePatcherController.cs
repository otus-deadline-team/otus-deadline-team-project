using Microsoft.AspNetCore.Mvc;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.App.Telegram.BL.Abstractions;
using Pms.Core.Client;
using Pms.Core.Client.ApiModels.Issue;
using PMS.Core.App.Infrastucture;
using PMS.Identity.Client.ApiModels.User;
using Pms.Core.App.Extensions;
using Pms.Core.App.Common;
using Pms.Core.App.SystemWorker.BL.Abstraction;

namespace Pms.Core.App.Issue.Controllers;

[ApiController]
public class IssuePatcherController : ControllerBase
{
    private readonly IIssuePatcher _issuePatcher;
    private readonly ICommonService _identityCommonService;
    private readonly IIssueReader _issueReader;
    private readonly ISystemWorker _systemWorker;

    public IssuePatcherController(IIssuePatcher issuePatcher, ICommonService identityCommonService, IIssueReader issueReader, ISystemWorker systemWorker)
    {
        _issuePatcher = issuePatcher;
        _identityCommonService = identityCommonService;
        _issueReader = issueReader;
        _systemWorker = systemWorker;
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System, Priviliges.User)]
    [HttpPatch(CoreRouting.Issues.Change)]
    public async Task<ActionResult<IssueApiModel>> ChangeIssue([FromQuery] Guid issueId,
        [FromBody] IssuePatchApiModel patchApiModel)
    {
        try
        {
            IssueApiModel curIssueInfo = _issueReader.GetIssueByIdAsync(issueId).Result;
            var result = await _issuePatcher.PatchAsync(issueId, patchApiModel);
            if(curIssueInfo.ResponsibleId != patchApiModel.ResponsibleId)
            {
                UserApiModel responsibleUser = _identityCommonService.Get<UserApiModel>($"user/{patchApiModel.ResponsibleId}");
                if (responsibleUser != null && !string.IsNullOrEmpty(responsibleUser.TelegramUserName))
                {
                    string textToResponsible = $"��� ��������� ������ {patchApiModel.Title}";
                    _systemWorker.SendMessage(textToResponsible, responsibleUser.TelegramUserName);
                }
            }
            return Ok(result);
        }
        catch (ArgumentException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(detail: ex.Message, statusCode: 500);
        }
    }

    [RequirePrivelege(Priviliges.Administrator, Priviliges.System, Priviliges.User)]
    [HttpPatch(CoreRouting.Issues.ChangeState)]
    public async Task<ActionResult<IssueApiModel>> ChangeIssueState([FromQuery] Guid issueId,
        [FromBody] IssuePatchApiModel patchApiModel)
    {
        try
        {
            var result = await _issuePatcher.ChangeStateAsync(issueId, patchApiModel.IssueState);
            if (patchApiModel.ResponsibleId != null)
            {
                UserApiModel responsibleUser = _identityCommonService.Get<UserApiModel>($"user/{patchApiModel.ResponsibleId}");
                if (responsibleUser != null && !string.IsNullOrEmpty(responsibleUser.TelegramUserName)) 
                {
                    string textToResponsible = $"������ {patchApiModel.Title} ���������� � ������ {patchApiModel.IssueState.GetDescription()}";
                    _systemWorker.SendMessage(textToResponsible, responsibleUser.TelegramUserName);
                }
            }
            return Ok(result);
        }
        catch (ArgumentException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(detail: ex.Message, statusCode: 500);
        }
    }
}