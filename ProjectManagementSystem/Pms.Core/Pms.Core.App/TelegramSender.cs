﻿using Newtonsoft.Json;
using Pms.Core.App.RabbitMQ;
using PMS.Notification.Client.ApiModels.Notification;

namespace Pms.Core.App
{
    public class TelegramSender
    {
        private readonly IRabbitMqService _mqService;

        public TelegramSender(IRabbitMqService mqService)
        {
            _mqService = mqService;
        }
        
        public void SendMessageToRabbit(string message,string tgName)
        {
            TgNotifRabbitMessage tg = new TgNotifRabbitMessage();
            tg.Message = message;
            tg.Receiver = tgName;

            _mqService.SendMessage(tg);
        }
    }
}
