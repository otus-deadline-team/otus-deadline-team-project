﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Pms.Core.App.Models.Enums;
using Pms.Core.App.Project.BL.Abstractions;
using Pms.Core.App.SystemWorker.BL.Abstraction;
using Pms.Core.App.Telegram.BL.Abstractions;
using Pms.Core.App.Project.BL.Impl;
using Pms.Core.Client;
using Pms.Core.Client.ApiModels.Project;
using PMS.Core.App.Infrastucture;

namespace Pms.Core.App.Project.Controllers;

[EnableCors("_myAllowSpecificOrigins")]
[ApiController]
public class ProjectCreateController : ControllerBase
{

    private readonly IProjectRepository _projectRepository;
    private readonly ISystemWorker _systemWorker;

    public ProjectCreateController(IProjectRepository projectRepository, ISystemWorker systemWorker)
    {
        _projectRepository = projectRepository;
        _systemWorker = systemWorker;
    }

    /// <summary>
    /// Get all projects
    /// </summary>
    /// <returns></returns>
    [RequirePrivelege(Priviliges.Administrator, Priviliges.System, Priviliges.Spectrator)]
    [HttpGet(CoreRouting.GetAllProject)]
    public async Task<ActionResult<List<ProjectApiModel>>> GetAllProjects()
    {
        var res = await _projectRepository.GetProjects();

        //По логике работы этой точки отправлять в телегу отсюда ничего не надо
        //var userInfo = User.Identities.FirstOrDefault();
        //if (userInfo != null)
        //{
        //    string nameTg = userInfo.Claims.FirstOrDefault(a => a.Type == "NameTelegram").Value;
        //    // Здесь отправляем в телеграм
        //    _telegramService.SendToTelegram(res?.ToString()??" ", nameTg);
        //}      
        
        Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
        Response.Headers.Add("X-Total-Count", res?.Count.ToString());
        return res;
       // return await _projectRepository.GetProjects();
    }


    /// <summary>
    /// Get project by id
    /// </summary>
    /// <param name="id">id</param>
    /// <returns></returns>
    [HttpGet(CoreRouting.GetProject)]
    [RequirePrivelege(Priviliges.Administrator, Priviliges.System, Priviliges.User, Priviliges.Spectrator)]
    public async Task<ActionResult<ProjectApiModel?>> GetProject([FromQuery] Guid id)
    {
        var project = await _projectRepository.GetProject(id);
        if (project != null)
            return Ok(project);
        else
            return NotFound();

    }

    /// <summary>
    /// Create new project
    /// </summary>
    /// <param name="project">New project</param>
    /// <returns></returns>
    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpPost(CoreRouting.CreateProject)]
    public async Task<ActionResult<ProjectApiModel>>CreateProject(string projectName)
    {
        var userInfo = User.Identities.FirstOrDefault();
        if (userInfo != null)
        {
            ProjectCreateApiModel newProjectData = new ProjectCreateApiModel
            {
                CreatedBy = userInfo.Name,
                CreatedById = new Guid(userInfo.Claims.FirstOrDefault(a => a.Type == "ID").Value),
                Name = projectName,
                CreatedAt = DateTimeOffset.UtcNow
            };
            var res = await _projectRepository.CreateAsync(newProjectData);
            return Ok(res);
        }
        else
        {
            return StatusCode(500);
        }
    }


    /// <summary>
    /// Patch project
    /// </summary>
    /// <param name="project">Project for patch</param>
    /// <returns></returns>
    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpPatch(CoreRouting.PatchProject)]
    public async Task<ActionResult> PatchProject([FromQuery] Guid id, [FromBody] ProjectPatchApiModel project)
    {
        var task = _projectRepository.PatchAsync(id, project);
        await task;

        if (task.Result == ActionEnum.WasDone)
            return Ok();
        else if (task.Result == ActionEnum.NotFound)
            return NotFound();
        else
            return StatusCode(500);
    }


    /// <summary>
    /// Delete project by id
    /// </summary>
    /// <param name="id">id</param>
    /// <returns></returns>
    [RequirePrivelege(Priviliges.Administrator, Priviliges.System)]
    [HttpDelete(CoreRouting.DeleteProject)]
    public async Task<ActionResult> DeleteProject([FromQuery] Guid id)
    {
        ActionEnum res = await _projectRepository.DeleteAsync(id);
        if (res == ActionEnum.WasDone)
        { 
            return Ok();
        }
        if (res == ActionEnum.NotFound)
        {
            return NotFound();
        }
        return StatusCode(500);
    }
}