﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Pms.Core.App.Context;
using Pms.Core.App.Models.Enums;
using Pms.Core.App.Project.BL.Abstractions;
using Pms.Core.Client.ApiModels.Project;

namespace Pms.Core.App.Project.BL.Impl;

public class ProjectRepository : IProjectRepository
{
    private readonly CoreContext _context;
    private readonly IMapper _mapper;
    public ProjectRepository(CoreContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<ProjectApiModel?> GetProject(Guid id)
    {
        try
        {
            var entity = await _context.Projects.FirstOrDefaultAsync(x => x.Id == id && !x.IsArchive);
            if (entity != null)
            {
                return _mapper.Map<ProjectApiModel>(entity);
            }
            else
                return null;
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public async Task<List<ProjectApiModel>> GetProjects()
    {
        try
        {
            var entities = await _context.Projects.Where(x=> !x.IsArchive).ToListAsync();
            return _mapper.Map<List<ProjectApiModel>>(entities);
        }
        catch (Exception ex)
        {
            throw;
        }
    }

    public async Task<ProjectApiModel> CreateAsync(ProjectCreateApiModel createApiModel)
    {
        try
        {
            Models.Entities.Project pro = _mapper.Map<Models.Entities.Project>(createApiModel);
            await _context.Projects.AddAsync(pro);
            await this._context.SaveChangesAsync();

            return _mapper.Map<ProjectApiModel>(pro);
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    
    public async Task<ActionEnum> PatchAsync(Guid id, ProjectPatchApiModel project)
    {
        try
        {
            Models.Entities.Project currentProject = await _context.Projects.FindAsync(id);
            
            if (currentProject == null)
            {
                return ActionEnum.NotFound;
                //throw new ArgumentException($"Project not found: {id}");
            }
            else
            {
                currentProject.Name = project.Name;
                await this._context.SaveChangesAsync();
                return ActionEnum.WasDone;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
    
    public async Task<ActionEnum> DeleteAsync(Guid id)
    {
        try
        {
            var project = await this._context.Projects.FindAsync(id);
            if (project == null)
                return ActionEnum.NotFound;
                    //throw new ArgumentException($"Project not found: {id}");
            else
            {
                project.IsArchive = true;
                _context.Projects.Update(project);
                var issues = _context.Issues.Where(x => x.ProjectId == project.Id);
                foreach (var issue in issues)
                {
                    issue.IsArchive = true;
                }
                _context.Issues.UpdateRange(issues);
                await this._context.SaveChangesAsync();
                return ActionEnum.WasDone;
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}