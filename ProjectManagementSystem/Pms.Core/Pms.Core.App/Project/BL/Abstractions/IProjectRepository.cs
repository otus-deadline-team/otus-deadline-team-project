﻿using Pms.Core.App.Models.Enums;
using Pms.Core.Client.ApiModels.Project;

namespace Pms.Core.App.Project.BL.Abstractions;

public interface IProjectRepository
{
    /// <summary>
    /// Get project by id
    /// </summary>
    /// <param name="id">id</param>
    /// <returns></returns>
    public Task<ProjectApiModel?> GetProject(Guid id);
    
    /// <summary>
    /// Get all projects
    /// </summary>
    /// <returns></returns>
    public Task<List<ProjectApiModel>> GetProjects();

    /// <summary>
    /// Add new project
    /// </summary>
    /// <param name="createApiModel">new project</param>
    /// <returns></returns>
    public Task<ProjectApiModel> CreateAsync(ProjectCreateApiModel createApiModel);


    /// <summary>
    /// Update project by id
    /// </summary>
    /// <param name="project">project</param>
    /// <returns></returns>
    public Task<ActionEnum> PatchAsync(Guid id, ProjectPatchApiModel project);
    
    /// <summary>
    /// Delete project by id
    /// </summary>
    /// <param name="id">id</param>
    /// <returns></returns>
    public Task<ActionEnum> DeleteAsync(Guid id);

}