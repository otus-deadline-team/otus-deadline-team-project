﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Pms.Core.App.Migrations
{
    public partial class AddDefaultDataSetInIssue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "core",
                table: "Issues",
                columns: new[] { "Id", "CreatedAt", "CreatedBy", "CreatedById", "Description", "IsArchive", "ParentIssueId", "ProjectId", "ResponsibleId", "State", "Title" },
                values: new object[,]
                {
                    { new Guid("8f6c9823-1a1e-4344-91cc-5d2e464e7085"), null, null, null, "Подробности задачи", false, null, new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"), null, "BACKLOG", "Тестовая задача 2" },
                    { new Guid("f4aa812b-f797-48f6-9f6c-08d25034cc4d"), null, null, null, "Подробности задачи", false, null, new Guid("0f8fad5b-d9cb-469f-a165-70867728950e"), null, "BACKLOG", "Тестовая задача 1" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "core",
                table: "Issues",
                keyColumn: "Id",
                keyValue: new Guid("8f6c9823-1a1e-4344-91cc-5d2e464e7085"));

            migrationBuilder.DeleteData(
                schema: "core",
                table: "Issues",
                keyColumn: "Id",
                keyValue: new Guid("f4aa812b-f797-48f6-9f6c-08d25034cc4d"));
        }
    }
}
