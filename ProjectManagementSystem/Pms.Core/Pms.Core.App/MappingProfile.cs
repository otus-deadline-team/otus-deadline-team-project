using AutoMapper;
using Pms.Core.App.Models.Enums;
using Pms.Core.Client.ApiModels.Issue;
using Pms.Core.Client.ApiModels.Project;

namespace Pms.Core.App;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<Models.Entities.Project, ProjectApiModel>();
        CreateMap<ProjectApiModel, Models.Entities.Project>();

        CreateMap<Models.Entities.Project, ProjectCreateApiModel>();
        CreateMap<ProjectCreateApiModel, Models.Entities.Project>();

        CreateMap<Models.Entities.Project, ProjectPatchApiModel>();
        CreateMap<ProjectPatchApiModel, Models.Entities.Project>();

        #region IssueConfiguration

        CreateMap<IssueStateApiEnum, IssueState>();
        CreateMap<IssueState, IssueStateApiEnum>();
        
        CreateMap<Models.Entities.Issue, IssueApiModel>();
        CreateMap<IssueApiModel, Models.Entities.Issue>();

        CreateMap<Models.Entities.Issue, IssueCreateApiModel>();
        CreateMap<IssueCreateApiModel, Models.Entities.Issue>();
        
        CreateMap<Models.Entities.Issue, IssuePatchApiModel>();
        CreateMap<IssuePatchApiModel, Models.Entities.Issue>();
        
        #endregion
    }
}