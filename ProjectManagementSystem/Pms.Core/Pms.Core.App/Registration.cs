using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using Pms.Core.App.Common;
using Pms.Core.App.Context;
using Pms.Core.App.Issue.BL.Abstractions;
using Pms.Core.App.Issue.BL.Impl;
using Pms.Core.App.Project.BL.Abstractions;
using Pms.Core.App.Project.BL.Impl;
using Pms.Core.App.RabbitMQ;
using Pms.Core.App.Telegram.BL.Abstractions;
using Pms.Core.App.Telegram.BL.Impl;
using PMS.Core.App.Infrastucture;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Xml.Linq;
using JwtBearerOptions = PMS.Core.App.Infrastucture.JwtBearerOptions;
using Pms.Core.App.SystemWorker.BL.Abstraction;

namespace Pms.Core.App;

public static class Registration
{
    public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(SwaggerOptions.SwaggerGenOptions(new SwaggerGenOptions(), configuration));
        services.AddDbContext<CoreContext>(opt => opt.UseNpgsql(configuration.GetConnectionString("ModuleDatabase"),
            x => x.MigrationsHistoryTable("__MigrationHistory", "core")));
        var mapperConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new MappingProfile());
        });
        IMapper mapper = mapperConfig.CreateMapper();
        services.AddSingleton(mapper);

        services.AddTransient<IProjectRepository, ProjectRepository>();
        services.AddTransient<IIssuePatcher, IssuePatcher>();
        services.AddTransient<IIssueCreator, IssueCreator>();
        services.AddTransient<IIssueReader, IssueReader>();
        services.AddTransient<IIssueRemover, IssueRemover>();
        //services.AddTransient<ITelegramService, TelegramService>();
        services.AddTransient<ICommonService, CommonService>();
        //Добавляем авторизацию
        IAuthOptions authOptions = new AuthOptions(configuration);
        services.AddSingleton(authOptions);
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(JwtBearerOptions.GetJwtBearerOptions(new Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerOptions(), configuration, authOptions));

        //Добавляем Rabbit
        IRabbitMQOptions rabbitOptions = new RabbitMQOptions(configuration);
        services.AddSingleton(rabbitOptions);

        services.AddSingleton<IRabbitMqService, RabbitMqService>();
        services.AddSingleton<ISystemWorker, Pms.Core.App.SystemWorker.BL.Impl.SystemWorker>();


        //При старте приложения запускаем миграции
        using var serviceProvider = services.BuildServiceProvider();
        using var context = serviceProvider.GetRequiredService<CoreContext>();
        context.Database.Migrate();

        services.AddHttpClient<ICommonService, CommonService>(client =>
        {
            client.BaseAddress = new Uri(configuration["IdentityUrl"]);
            client.Timeout = new TimeSpan(0, 0, 30);
            client.DefaultRequestHeaders.Clear();
        }).ConfigurePrimaryHttpMessageHandler(() =>
        {
            return new HttpClientHandler()
            {
                UseDefaultCredentials = true
            };
        })
        .SetHandlerLifetime(TimeSpan.FromMinutes(2));
    }
}