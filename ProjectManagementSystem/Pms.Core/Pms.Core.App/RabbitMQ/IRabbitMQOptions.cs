﻿using RabbitMQ.Client;

namespace Pms.Core.App.RabbitMQ
{
    public interface IRabbitMQOptions
    {
        ConnectionFactory Factory { get; set; }

        string TgQueueName { get; set; }
    }
}