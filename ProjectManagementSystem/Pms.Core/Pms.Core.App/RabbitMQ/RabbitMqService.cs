﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;
using PMS.Notification.Client.ApiModels.Notification;
using RabbitMQ.Client;
using System.Runtime.CompilerServices;
using System.Text;

namespace Pms.Core.App.RabbitMQ
{
    public class RabbitMqService : IRabbitMqService
    {
        private readonly IRabbitMQOptions _rabbitMQOptions;
        
        public RabbitMqService(IRabbitMQOptions rabbitMQOptions) : base()
        {
            _rabbitMQOptions = rabbitMQOptions;
        }
        
        public void SendMessage(object obj)
        {
            var message = JsonConvert.SerializeObject(obj);
            SendMessage(message);
        }

        public void SendMessage(string message)
        {
            using (var connection = _rabbitMQOptions.Factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _rabbitMQOptions.TgQueueName,
                               durable: true,// false падал на стороне кролика
                               exclusive: false,
                               autoDelete: false,
                               arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                               routingKey: _rabbitMQOptions.TgQueueName,
                               basicProperties: null,
                               body: body);
            }
        }
    }
}
