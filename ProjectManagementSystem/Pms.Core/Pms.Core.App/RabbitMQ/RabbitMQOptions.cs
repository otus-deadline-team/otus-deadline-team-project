﻿using RabbitMQ.Client;

namespace Pms.Core.App.RabbitMQ
{
    public class RabbitMQOptions : IRabbitMQOptions
    {
        private ConnectionFactory _factory;
        /// <summary>
        /// Коннект
        /// </summary>
        public ConnectionFactory Factory
        {
            get { return _factory; }
            set { _factory = value; }
        }

        private string _TgQueueName;
        /// <summary>
        /// Имя очереди в рэббите
        /// </summary>
        public string TgQueueName
        {
            get { return _TgQueueName; }
            set { _TgQueueName = value; }
        }

        public RabbitMQOptions(IConfiguration configuration) : base()
        {
            _factory = new ConnectionFactory()
            {
                UserName = configuration.GetSection("RabbitMQ:ConnectionFactoryOptions:UserName").Value, //"prvtowue",
                Password = configuration.GetSection("RabbitMQ:ConnectionFactoryOptions:Password").Value,
                VirtualHost = configuration.GetSection("RabbitMQ:ConnectionFactoryOptions:VirtualHost").Value,
                HostName = configuration.GetSection("RabbitMQ:ConnectionFactoryOptions:HostName").Value
            };

            _TgQueueName = configuration.GetSection("RabbitMQ:TelegrammQueueName:Name").Value;
        }
    }
}
