﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Pms.Core.App.RabbitMQ;
using PMS.Notification.Client.ApiModels.Notification;
using System.Net.Http;
using System.Text;

namespace Pms.Core.App
{
    public class MiddlewareCore
    {
        private readonly RequestDelegate _next;


        public MiddlewareCore(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                var originBody = context.Response.Body;
                using MemoryStream memStream = new MemoryStream();
                context.Response.Body = memStream;

                try
                {
                    await _next(context);

                    memStream.Position = 0;
                    var responseBody = new StreamReader(memStream).ReadToEnd();

                    using MemoryStream memoryStreamModified = new MemoryStream();
                    var sw = new StreamWriter(memoryStreamModified);
                    sw.Write(responseBody);
                    sw.Flush();
                    memoryStreamModified.Position = 0;

                    await memoryStreamModified.CopyToAsync(originBody).ConfigureAwait(false);
                    context.Response.Body = originBody;
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = 500;
                    context.Response.Headers.Append("Error", ex.Message);
                    context.Response.ContentType = "text/xml";

                    memStream.Position = 0;
                    
                    using MemoryStream memoryStreamModified = new MemoryStream();
                    var sw = new StreamWriter(memoryStreamModified);
                    sw.Write(ex.Message);
                    sw.Flush();
                    memoryStreamModified.Position = 0;
                                    
                    byte[] bytes = Encoding.ASCII.GetBytes(ex.Message);                   
                    context.Response.ContentLength = bytes.Length;

                    await memoryStreamModified.CopyToAsync(originBody).ConfigureAwait(false);
                    context.Response.Body = originBody;  
                }
            }
            catch
            {
                context.Response.StatusCode = 500;
            }
        }
    }

}
