﻿namespace Pms.Core.App.Common
{
    /// <summary>
    /// Класс для обращения к другим приложениям
    /// </summary>
    public interface ICommonService
    {
        T Get<T>(string apiUri);
        List<T> GetMany<T>(string apiUri);
        T Post<T>(string apiUri, object param);
        List<T> PostMany<T>(string apiUri, object param);
    }
}
