﻿using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace Pms.Core.App.Common
{
    public class CommonService : ICommonService
    {
        private readonly HttpClient _client;
        public CommonService(HttpClient client)
        {
            _client = client;
        }
        /// <summary>
        /// Выполняет Get запрос и возвращает единственный объект.
        /// </summary>
        /// <param name="apiUri">URI запроса. Формат: {имя контроллера}/{имя метода}{?параметры}</param>
        /// <returns>Объект типа T</returns>
        public T Get<T>(string apiUri)
        {
            var response = _client.GetAsync(apiUri).Result;
            return ProcessResponse<T>(response);
        }
        /// <summary>
        /// Выполняет Get запрос и возвращает список.
        /// </summary>
        /// <param name="apiUri">URI запроса. Формат: {имя контроллера}/{имя метода}{?параметры}</param>
        /// <returns>Список объектов типа T</returns>
        public List<T> GetMany<T>(string apiUri)
        {
            var response = _client.GetAsync(apiUri).Result;
            return ProcessResponseMany<T>(response);
        }
        /// <summary>
        /// Выполняет Post запрос и возвращает единственный объект.
        /// </summary>
        /// <param name="apiUri">URI запроса. Формат: {имя контроллера}/{имя метода}</param>
        /// <param name="param">Объект с параметрами</param>
        /// <returns>Объект типа T</returns>
        public T Post<T>(string apiUri, object param)
        {
            var requestBody = new StringContent(JsonConvert.SerializeObject(param), Encoding.UTF8, "application/json");
            var response = _client.PostAsync(apiUri, requestBody).Result;
            return ProcessResponse<T>(response);
        }
        /// <summary>
        /// Выполняет Post запрос и возвращает список.
        /// </summary>
        /// <param name="apiUri">URI запроса. Формат: {имя контроллера}/{имя метода}</param>
        /// <param name="param">объект с параметрами</param>
        /// <returns>Список объектов типа T</returns>
        public List<T> PostMany<T>(string apiUri, object param)
        {
            var requestBody = new StringContent(JsonConvert.SerializeObject(param), Encoding.UTF8, "application/json");
            var response = _client.PostAsync(apiUri, requestBody).Result;
            return ProcessResponseMany<T>(response);
        }
        /// <summary>
        /// Обрабатывает полученный результат
        /// </summary>
        /// <param name="response"></param>
        /// <returns>Объект типа T</returns>
        public T ProcessResponse<T>(HttpResponseMessage response)
        {
            CheckResponseStatusCode(response);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataFromBoss = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<T>(dataFromBoss);
            }

            return default(T);
        }
        /// <summary>
        /// Обрабатывает полученный результат
        /// </summary>
        /// <param name="response"></param>
        /// <returns>Список объектов типа T</returns>
        public List<T> ProcessResponseMany<T>(HttpResponseMessage response)
        {
            CheckResponseStatusCode(response);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var dataFromBoss = response.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<List<T>>(dataFromBoss);
            }

            return new List<T>();
        }
        /// <summary>
        /// Проверяет статус ответа.
        /// </summary>
        /// <param name="response"></param>
        private void CheckResponseStatusCode(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                var erorrDataFromBoss = response.Content.ReadAsStringAsync().Result;
                throw new HttpRequestException(JsonConvert.DeserializeObject<string>(erorrDataFromBoss));
            }
        }
    }
}
