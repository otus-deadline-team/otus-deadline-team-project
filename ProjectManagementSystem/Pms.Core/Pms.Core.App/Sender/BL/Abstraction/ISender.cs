﻿namespace Pms.Core.App.Sender.BL.Abstraction
{
    public interface ISender
    {
        /// <summary>
        /// Отправляем сообщения
        /// </summary>
        /// <param name="obj">что отправляем</param>
        /// <param name="tgName">кому отправляем</param>
        public void Send(object obj, string tgName);
    }
}
