﻿using Newtonsoft.Json;
using Pms.Core.App.RabbitMQ;
using Pms.Core.App.Sender.BL.Abstraction;
using PMS.Notification.Client.ApiModels.Notification;

namespace Pms.Core.App.Sender.BL.Impl
{
    /// <summary>
    /// Отправляет сообщения через рэббит в телеграм
    /// </summary>
    public class SenderToRabbit : ISender
    {
        private readonly IRabbitMqService _mqService;

        public SenderToRabbit(IRabbitMqService mqService)
        {
            _mqService = mqService;
        }

        /// <summary>
        /// Отправить cообщение через рэббит в телеграм
        /// </summary>
        /// <param name="obj">Сообщение</param>
        /// <param name="tgName">Адресат</param>
        public void Send(object obj, string tgName)
        {
            string message = JsonConvert.SerializeObject(obj);

            TgNotifRabbitMessage tg = new TgNotifRabbitMessage();
            tg.Message = message;
            tg.Receiver = tgName;

            _mqService.SendMessage(tg);
        }
    }
}
