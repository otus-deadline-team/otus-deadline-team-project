﻿using Pms.Core.App.Sender.BL.Abstraction;

namespace Pms.Core.App.Sender.BL.Impl
{
    /// <summary>
    /// Отправляет сообщения почтой
    /// </summary>
    public class SenderToMail : ISender
    {
        public SenderToMail()
        {
        }

        public void Send(object obj, string tgName)
        {
            /// Реализация отправки почтой
        }
    }
}
