﻿using Pms.Core.App.Sender.BL.Abstraction;

namespace Pms.Core.App.SystemWorker.BL.Abstraction
{
    public interface ISystemWorker
    {
        /// <summary>
        /// Список сендеров
        /// </summary>
        List<ISender> SendingList { get; set; }

        /// <summary>
        /// Отправить cообщения по всем сендерам из списка
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="name">Адресат</param>
        void SendMessage(object message, string name);

    }
}
