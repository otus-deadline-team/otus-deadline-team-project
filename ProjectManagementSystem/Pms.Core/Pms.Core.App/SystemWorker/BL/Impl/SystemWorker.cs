﻿using Pms.Core.App.RabbitMQ;
using Pms.Core.App.Sender.BL.Abstraction;
using Pms.Core.App.Sender.BL.Impl;
using Pms.Core.App.SystemWorker.BL.Abstraction;

namespace Pms.Core.App.SystemWorker.BL.Impl
{
    public class SystemWorker : ISystemWorker
    {
        /// <summary>
        /// Список рассылок
        /// </summary>
        public List<ISender> SendingList { get; set; }

        public SystemWorker(IRabbitMqService mqService)
        {
            SendingList = new List<ISender>();

            SendingList.Add(new SenderToRabbit(mqService)); //добавляем рассылку в телеграм через рэббит
            SendingList.Add(new SenderToMail());            //добавляем рассылку в почту (не реализовано)
        }

        /// <summary>
        /// Отправить cообщения по всем сендерам из списка
        /// </summary>
        /// <param name="message">Сообщение</param>
        /// <param name="name">Адресат</param>
        public void SendMessage(object message, string name)
        {
            foreach (ISender sender in SendingList)
                sender.Send(message, name);
        }
    }
}
