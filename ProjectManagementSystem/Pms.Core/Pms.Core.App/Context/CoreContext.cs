using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Pms.Core.App.Models.EntityConfigurations;

namespace Pms.Core.App.Context;

public class CoreContext : DbContext
{
    public CoreContext(DbContextOptions<CoreContext> options) : base(options)
    { }

    public DbSet<Models.Entities.Project> Projects { get; set; }
    public DbSet<Models.Entities.Issue> Issues { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("core");
        modelBuilder.UseSerialColumns();
        modelBuilder.ApplyConfiguration(new ProjectConfiguration());
        modelBuilder.ApplyConfiguration(new IssueConfigurations());
    }
    
}