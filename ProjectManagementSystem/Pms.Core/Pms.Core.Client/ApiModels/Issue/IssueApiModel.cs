namespace Pms.Core.Client.ApiModels.Issue;

public class IssueApiModel
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public IssueStateApiEnum IssueState { get; set; }
    public Guid? ResponsibleId { get; set; }
    public Guid? ParentIssueId { get; set; }
    public Guid? ProjectId { get; set; }
    public List<IssueApiModel> SubIssueApiModels { get; set; }
}