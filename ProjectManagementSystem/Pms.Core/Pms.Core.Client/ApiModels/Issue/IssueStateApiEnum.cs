using System.ComponentModel;

namespace Pms.Core.Client.ApiModels.Issue;

public enum IssueStateApiEnum
{
    [Description("������")]
    BACKLOG,
    [Description("� ��������")]
    IN_PROGRESS,
    [Description("���������")]
    COMPLETE
}