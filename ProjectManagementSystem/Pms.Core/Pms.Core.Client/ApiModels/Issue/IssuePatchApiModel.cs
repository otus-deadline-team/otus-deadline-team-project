namespace Pms.Core.Client.ApiModels.Issue;

public class IssuePatchApiModel
{
    
    public string Title { get; set; }
    public string Description { get; set; }
    public IssueStateApiEnum IssueState { get; set; }
    public Guid? ResponsibleId { get; set; }
    public Guid? ParentIssueId { get; set; }
    public Guid? ProjectId { get; set; }
}