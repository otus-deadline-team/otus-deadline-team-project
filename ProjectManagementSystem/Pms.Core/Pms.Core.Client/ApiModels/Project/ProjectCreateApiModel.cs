﻿namespace Pms.Core.Client.ApiModels.Project;

public class ProjectCreateApiModel
{
    public string Name { get; set; }
    public Guid CreatedById { get; set; }
    public string CreatedBy { get; set; } 
    public DateTimeOffset? CreatedAt { get; set; }
}