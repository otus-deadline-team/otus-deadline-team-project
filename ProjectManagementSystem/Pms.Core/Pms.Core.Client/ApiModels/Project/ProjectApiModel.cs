﻿namespace Pms.Core.Client.ApiModels.Project;

public class ProjectApiModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string CreatedBy { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
}