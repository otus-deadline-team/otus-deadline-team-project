﻿using Pms.Core.Client.ApiModels.Project;

namespace Pms.Core.Client;

public class CoreClient
{
    private HttpClient _client;

    public CoreClient(HttpClient client)
    {
        _client = client;
    }

    // public async Task<ProjectApiModel> CreateProjectAsync(HttpRequestMessage requestMessage,
    //     CancellationToken cancellationToken = default)
    // {
    //     return await _client.SendAsync(requestMessage, cancellationToken);
    // }
}