﻿namespace Pms.Core.Client;

public static class CoreRouting
{
    public const string GetAllProject = "projects";
    public const string GetProject = "project/{id}";
    public const string CreateProject = "project";
    public const string PatchProject = "project/{id}";
    public const string DeleteProject = "project/{id}";
    
    public static class Issues
    {
        public const string Get = "issue/{issueId}";
        public const string GetAllByProjectId = "issue/project/{projectId}";
        public const string Create = "issue";
        public const string Change = "issue/{issueId}";
        public const string ChangeState = "issue/{issueId}/change-state";
        public const string Remove = "issue/{issueId}";
    } 
}